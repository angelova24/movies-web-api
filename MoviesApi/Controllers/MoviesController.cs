﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models.Data;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Character;
using MoviesApi.Models.DTOs.Movie;


namespace MoviesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private IMapper _mapper;

        public MoviesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of movies with characters in that movie
        /// </summary>
        /// <returns></returns>
        // GET: Movies
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movieList = _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Include(m => m.Characters).ToListAsync<Movie>());

            return Ok(movieList);
        }

        /// <summary>
        /// Gets a movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Movies/Details/5       
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            var movie = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (!MovieExists(id))
            {
                return NotFound();
            }

            MovieReadDTO movieToReturn = _mapper.Map<MovieReadDTO>(movie);
            return Ok(movieToReturn);
        }

        /// <summary>
        /// Updates a movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        // PUT: api/Movies/5       
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateMovie(int id, [FromBody] MovieUpdateDTO newMovie)
        {
            if (id != newMovie.Id)
            {
                return BadRequest();
            }
            var domainProf = _mapper.Map<Movie>(newMovie);
            _context.Entry(domainProf).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        // POST: Movies/Create        
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie([FromBody]MovieCreateDTO newMovie)
        {
            var domainMovie = _mapper.Map<Movie>(newMovie);
            _context.Movies.Add(domainMovie);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieById", new { id = domainMovie.Id }, newMovie);
        }

        /// <summary>
        /// Updates/Adds a character in a movie
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characters">Id of characters</param>
        /// <returns></returns>
        [HttpPost("Character/{id}")]
        public async Task<IActionResult> UpdateCharacterInMovie(int id, List<int> characters)
        {

            var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(c => c.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            foreach (var charactersid in characters)
            {
                var tempCharacter = await _context.Characters.FirstOrDefaultAsync(c => c.Id == charactersid);
                if (tempCharacter != null)
                {
                    movie.Characters.Add(tempCharacter);
                }

            }

            await _context.SaveChangesAsync();

            return NoContent();
        }


        /// <summary>
        /// Deletes a movie by ID
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (!MovieExists(id))
            {
                return NotFound();
            }
             
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }




        /// <summary>
        /// Checks for the movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

    }
}
