﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models.Data;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Character;
using MoviesApi.Models.DTOs.Franchise;
using MoviesApi.Models.DTOs.Movie;

namespace MoviesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets a list of all franchises in DB
        /// </summary>
        /// <returns>List with all franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Gets a franchise by Id
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            var franchiseReadDTO = _mapper.Map<FranchiseReadDTO>(franchise);

            return franchiseReadDTO;
        }

        // GET: api/Franchises/5/movies
        /// <summary>
        /// Returns all movies in a franchise by frnachise id
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetFranchisesMoviesById(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            var moviesResult = new List<MovieReadDTO>();

            foreach (var movie in franchise.Movies)
            {
                var movieDto = _mapper.Map<MovieReadDTO>(await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m == movie));
                moviesResult.Add(movieDto);
            }

            return moviesResult;
        }

        // GET: api/Franchises/5/characters
        /// <summary>
        /// Returns all characters in a franchise by franchise id
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllCharactersInFranchiseById(int id)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            var movies = await _context.Movies.Include(m => m.Characters).Where(m => m.FranchiseId == id).ToListAsync();

            if (movies.Count == 0)
            {
                return NoContent();
            }

            var characters = new HashSet<Character>();
            movies.ForEach(m => characters.UnionWith(m.Characters));

            return _mapper.Map<List<CharacterReadDTO>>(characters);
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Updates franchise info
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <param name="franchise">Franchise object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, FranchiseUpdateDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            var domainFranchise = _mapper.Map<Franchise>(franchise);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/Franchises/5/movies
        /// <summary>
        /// Updates/Adds movies to a franchise
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <param name="movies">List of movies id</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            foreach (var movieId in movies)
            {
                var movie = await _context.Movies.FirstOrDefaultAsync(m => m.Id == movieId);
                if (movie != null)
                {
                    franchise.Movies.Add(movie);
                }

            }
            var domainFranchise = _mapper.Map<Franchise>(franchise);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// Adds a new franchise to DB
        /// </summary>
        /// <param name="newFranchise">New Franchise object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO newFranchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(newFranchise);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchiseById", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes a franchise from DB
        /// </summary>
        /// <param name="id">Id of a franchise</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
