﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models.Data;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Character;

namespace MoviesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets a list of all characters in DB
        /// </summary>
        /// <returns>List with all characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.Include(c => c.Movies).ToListAsync());
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets a character by Id
        /// </summary>
        /// <param name="id">Id of a character</param>
        /// <returns>Character object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            var character = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == id).FirstOrDefaultAsync();

            if (character == null)
            {
                return NotFound();
            }
            
            var characterReadDTO = _mapper.Map<CharacterReadDTO>(character);

            return characterReadDTO;
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Updates characters info
        /// </summary>
        /// <param name="id">Id of a character</param>
        /// <param name="character">Character object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCharacter(int id, CharacterUpdateDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            var domainCharacter = _mapper.Map<Character>(character);

            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        /// <summary>
        /// Adds a new character to DB
        /// </summary>
        /// <param name="newCharacter">New Character object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO newCharacter)
        {
            var domainCharacter = _mapper.Map<Character>(newCharacter);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetCharacterById", new {id = domainCharacter.Id}, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes a character from DB
        /// </summary>
        /// <param name="id">Id of a character</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
