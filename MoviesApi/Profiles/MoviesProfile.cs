﻿using AutoMapper;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Movie;
using System.Linq;

namespace MoviesApi.Profiles
{
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
            CreateMap<Movie, MovieCreateDTO>()               
                .ReverseMap();
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt =>
               opt.MapFrom(m => m.Characters.Select(c => c.Id).ToArray()))
               .ReverseMap();
            CreateMap<Movie, MovieUpdateDTO>()
               .ReverseMap();
            CreateMap<Movie, MovieDeleteDTO>()
                .ReverseMap();
        }
    }
}
