﻿using AutoMapper;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Character;
using System.Linq;

namespace MoviesApi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt =>
               opt.MapFrom(c => c.Movies.Select(m => m.Id).ToArray()))
               .ReverseMap();
            CreateMap<Character, CharacterCreateDTO>()
                    .ReverseMap();
            CreateMap<Character, CharacterUpdateDTO>()
                    .ReverseMap();
            CreateMap<CharacterReadDTO, CharacterCreateDTO>()
                    .ReverseMap();
        }
    }
}
