﻿using AutoMapper;
using MoviesApi.Models.Domain;
using MoviesApi.Models.DTOs.Franchise;
using System.Linq;

namespace MoviesApi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt =>
               opt.MapFrom(f => f.Movies.Select(m => m.Id).ToArray()))
               .ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();
            CreateMap<Franchise, FranchiseUpdateDTO>()
                    .ReverseMap();
            CreateMap<FranchiseReadDTO, FranchiseCreateDTO>()
                    .ReverseMap();
        }
    }
}
