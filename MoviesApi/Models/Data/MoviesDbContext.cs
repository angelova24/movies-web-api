﻿using Microsoft.EntityFrameworkCore;
using MoviesApi.Models.Domain;
using System.Diagnostics.CodeAnalysis;

namespace MoviesApi.Models.Data
{
    public class MoviesDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        public MoviesDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Franchise>()
                .HasData(
                new Franchise() { Id = 1, Name = "Marvel Cinematic Universe" },
                new Franchise() { Id = 2, Name = "Harry Potter" });


            modelBuilder.Entity<Character>()
                .HasData(
                new Character() { Id = 1, FullName = "Robert Downey Jr.", Gender = "male" },
                new Character() { Id = 2, FullName = "Gwyneth Paltrow", Gender = "female" },
                new Character() { Id = 3, FullName = "Daniel Radcliffe", Gender = "male", PictureURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Daniel_Radcliffe_SDCC_2014.jpg/220px-Daniel_Radcliffe_SDCC_2014.jpg" },
                new Character() { Id = 4, FullName = "Emma Watson", Gender = "female" });


            modelBuilder.Entity<Movie>()
                .HasData(
                new Movie() { Id = 1, Title = "Iron Man", Genre = "Action, Fantasy", ReleaseYear = 2008, Director = "Jon Favreau", FranchiseId = 1 },
                new Movie() { Id = 2, Title = "Iron Man 2", Genre = "Action, Fantasy", ReleaseYear = 2010, Director = "Jon Favreau", FranchiseId = 1 },
                new Movie() { Id = 3, Title = "Harry Potter and the Philosopher's Stone", Genre = "Fantasy", ReleaseYear = 2001, Director = "Chris Columbus", FranchiseId = 2 },
                new Movie() { Id = 4, Title = "Harry Potter and the Chamber of Secrets", Genre = "Fantasy", ReleaseYear = 2002, Director = "Chris Columbus", FranchiseId = 2 });

            modelBuilder.Entity("CharacterMovie").HasData(
                new { CharactersId = 1, MoviesId = 1 },
                new { CharactersId = 1, MoviesId = 2 },
                new { CharactersId = 2, MoviesId = 1 },
                new { CharactersId = 2, MoviesId = 2 },
                new { CharactersId = 3, MoviesId = 3 },
                new { CharactersId = 3, MoviesId = 4 },
                new { CharactersId = 4, MoviesId = 3 },
                new { CharactersId = 4, MoviesId = 4 });

            modelBuilder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(p => p.Movies)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
