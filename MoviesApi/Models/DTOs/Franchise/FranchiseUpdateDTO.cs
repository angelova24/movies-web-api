﻿using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.DTOs.Franchise
{
    public class FranchiseUpdateDTO
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Description { get; set; }
    }
}
