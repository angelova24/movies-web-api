﻿using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.DTOs.Movie
{
    public class MovieUpdateDTO
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Title { get; set; }
        [Required, MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string PictureURL { get; set; }
        [MaxLength(200)]
        public string TrailerURL { get; set; }
    }
}
