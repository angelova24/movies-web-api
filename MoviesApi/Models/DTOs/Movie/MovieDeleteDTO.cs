﻿using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.DTOs.Movie
{
    public class MovieDeleteDTO
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Title { get; set; }

    }
}
