﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.DTOs.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        [Required, MaxLength(70)]
        public string FullName { get; set; }

        [MaxLength(20)]
        public string Alias { get; set; }

        [Required, MaxLength(10)]
        public string Gender { get; set; }

        [MaxLength(200)]
        public string PictureURL { get; set; }
        public List<int> Movies { get; set; }
    }
}
