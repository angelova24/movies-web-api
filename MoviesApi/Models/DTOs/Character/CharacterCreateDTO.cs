﻿using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.DTOs.Character
{
    public class CharacterCreateDTO
    {

        [Required, MaxLength(70)]
        public string FullName { get; set; }

        [MaxLength(20)]
        public string Alias { get; set; }

        [Required, MaxLength(10)]
        public string Gender { get; set; }

        [MaxLength(200)]
        public string PictureURL { get; set; }
    }
}
