﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Title { get; set; }
        [Required, MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string PictureURL { get; set; }
        [MaxLength(200)]
        public string TrailerURL { get; set; }

        //FK and navigation property
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        //navigation property
        public ICollection<Character> Characters { get; set; } = new HashSet<Character>();
    }
}
