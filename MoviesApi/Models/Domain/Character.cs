﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.Domain
{
    public class Character
    {
        public int Id { get; set; }

        [Required, MaxLength(70)]
        public string FullName { get; set; }
        [MaxLength(20)]
        public string Alias { get; set; }

        [Required, MaxLength(10)]
        public string Gender { get; set; }
        [MaxLength(200)]
        public string PictureURL { get; set; }
        //navigation property
        public ICollection<Movie> Movies { get; set; } = new HashSet<Movie>();
    }
}
