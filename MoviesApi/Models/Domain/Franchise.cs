﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesApi.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Description { get; set; }

        //navigation property
        public ICollection<Movie> Movies { get; set; } = new HashSet<Movie>();
    }
}
