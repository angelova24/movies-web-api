# Movies Web API

An Entity Framework Code First workflow and an ASP.NET Core Web API in C#.

## Description
The current project is an example Web API for learning purposes.\
Storing characters, movies and franchises.

## Table of Contents

- [Getting started](#getting-started)
- [Installation](#installation)
- [Support](#support)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [Project status](#project-status)


## Getting started

1. Clone the repo
```
git clone https://gitlab.com/angelova24/sql-assignment.git
```
2. Open solution in Visual Studio
3. Change the DB source in the **appsettings.json** file => "DefaultConnection": "Data Source = **@YourSQLServerName** ..";
4. Run migrations using Package Manager Console => this will create a database in your local DB
```
update-database
```
5. Run the project in Visual Studio to see and test the Web API endpoints in Swagger


## Installation
Make sure you have installed at least the following tools:
```
• Visual Studio 2019
• .NET 5
• SQL Server Management Studio
```


## Support
You can get support from the Authors of this repo.


## Authors and acknowledgment
@angelova24 and @Petar-Programing-Petrov


## Project status
Works as expected and taking in mind the expectations and requirements of the assignment.
